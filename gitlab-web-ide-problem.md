# GitLabのWeb IDEで日本語のファイル名が文字化けする不具合の対処法

GitLabのWeb IDEで日本語のファイル名が文字化けするする不具合があるので、その対処法を解説します。

## 動作確認した環境

GitLab CE 10.8.4 omunibus package

## 不具合の内容

リポジトリ内のファイル名が日本語の場合に、Web IDEに表示されるファイル名が文字化けし、ファイルの表示・編集ができません。
例えば、「テスト.md」というファイルは「"/343/203/206/343/202/271/343/203/210.md"」のように表示されます。

![Web IDE problem](images/web_ide_problem.png)

## 対処法

GitLabが内部的に使用している、フィーチャーフラグの `gitaly_ls_files` を有効にすることで、この不具合を回避することができます。
なお、クラウドサービスのGitLab.comではこのフラグが有効になっているため、日本語ファイル名の文字化けは発生しません。

具体的には、GitLabサーバーにSSHでログインして、以下のコマンドを実行してください。

```bash
$ sudo gitlab-rails console
> Feature.enable(:gitaly_ls_files)
> exit
```

- 参考: https://docs.gitlab.com/ee/development/gitaly.html

## 最後に

GitLabは海外製のプロダクトのため、日本語のファイル名を使用した場合などに思いもよらない不具合に遭遇する場合があります。
GitLabには公式の[Issue Tracker](https://gitlab.com/gitlab-org/gitlab-ce/issues)があるので、不具合に遭遇した場合は、同様の不具合が報告されていないか検索してみましょう。
もしIssueが見つからない場合は、GitLabの改善にもつながるので、ぜひ不具合の報告に挑戦してみてください。

なお、今回のケースでは既に[Issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/47893)が報告されていたので、私の方で対処法を調査してコメントとして記載しました。

また、クリエーションラインは、日本で唯一のGitLab Authorized Resellerとしてエンタープライズエディションのサブスクリプションの販売や日本語でのサポートを提供しています。
GitLabに関する詳細・お問い合わせは[こちら](https://www.creationline.com/gitlab)を参照ください。
