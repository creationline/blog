# GitLabのバックアップを取ってみた#gitlab


GitLabは毎月22日に新しいバージョンをリリースしています。  
リリースされた新しい機能を実装するためには、データのバックアップ＋バージョンアップは運用面の重要なポイントとなり、避けて通れません。  
今回は、Vagrant＋VirtualBoX環境にOmnibusインストールで構築したGitLabサーバのバックアップを取得する手順をご紹介します。  

### バックアップを取得する環境
・CentOS7.5  
・Gitlab11.0.3-ee.0.el7(Enterprise Edition)  
データは入っておらず、空のレポジトリ「panda」を1つ作成しました。  


### バックアップ情報の種類
GitLabのバックアップ対象は2種類に分かれています  
(1)GitLab構成情報  
(2)GitLabアプリケーションデータ  

(1)は該当のディレクトリをtarで固めて退避、(2)はGitLabで用意されているコマンドを実行することで、バックアップを取得します。  

## (1)GitLab構成情報のバックアップ取得
デフォルトでインストールされている場合、構成情報は/etc/gitlabに格納されています。  
バックアップファイルを格納する任意のディレクトリで以下を実行します。  

```bash
$ sudo tar cvzf /home/vagrant/yyyymmdd.tar.gz -C /etc gitlab　
```


(実行例)
```bash
$ sudo tar cvzf /home/vagrant/yyyymmdd.tar.gz -C /etc gitlab
gitlab/
gitlab/gitlab-secrets.json
gitlab/trusted-certs/
gitlab/gitlab.rb
```

出来上がったtarファイルのサイズは約30KBでした。


## (2)アプリケーションデータのバックアップ取得

次に、アプリケーションデータのバックアップを行います。
アプリケーションデータのバックアップのために、GitLabではコマンドが用意されています。

```bash
$ sudo gitlab-rake gitlab:backup:create
```

(実行例)
```bash
$ sudo gitlab-rake gitlab:backup:create
Dumping database ...
Dumping PostgreSQL database gitlabhq_production ... [DONE]
done
Dumping repositories ...
 * root/panda ... [SKIPPED]
 * root/panda.wiki ...  [SKIPPED]
done
Dumping uploads ...
done
Dumping builds ...
done
Dumping artifacts ...
done
Dumping pages ...
done
Dumping lfs objects ...
done
Dumping container registry images ...
[DISABLED]
Creating backup archive: 1531369164_2018_07_12_11.0.3-ee_gitlab_backup.tar ... done
Uploading backup archive to remote storage  ... skipped
Deleting tmp directories ... done
done
done
done
done
done
done
done
Deleting old backups ... skipping
```


コマンドを実行後、デフォルトの設定だと/var/opt/gitlab/backups以下にtarファイルができるので、それが生成されているかを確認しましょう。

## バックアップファイルの保存先変更

tarファイルの格納先を指定したい場合は、/etc/gitlab/gitlab.rb中の「gitlab_rails['backup_path']」を書き換えます

試しに、デフォルト保管先以外の場所を指定してみます。
```bash
$ sudo vi gitlab.rb
```
でファイルを編集モードにし、以下のように修正し、保存します。


(保存前)
```bash
### Backup Settings
###! Docs: https://docs.gitlab.com/omnibus/settings/backups.html

# gitlab_rails['manage_backup_path'] = true
# gitlab_rails['backup_path'] = "/var/opt/gitlab/backups"
```
　　　　　　　↓　　↓　　↓

(保存後)
```bash
### Backup Settings
###! Docs: https://docs.gitlab.com/omnibus/settings/backups.html

 gitlab_rails['manage_backup_path'] = true
 gitlab_rails['backup_path'] = "/var/opt/gitlab/backups2"
```


```bash
$ sudo gitlab-ctl reconfigure
$ sudo gitlab-rake gitlab:backup:create
```
でバックアップを実行すると、/var/opt/gitlab/backups2にtarファイルが生成されました！

この時、gitlab-rake gitlab:backup:createを実行する前に予め/var/opt/gitlab/backups2のディレクトリを作成し、  
パーミッションもデフォルトの格納先と合わせておいたのですが、存在しないディレクトリを指定した場合の挙動が気になりました。


そこで、バックアップ実行前に存在しないディレクトリ(/var/opt/gitlab/backups3)を指定してみたところ、  
ファイル編集後に実行するgitlab-ctl reconfigureの処理の中でディレクトリを作成していました。(※)  

reconfigureの標準出力から抜粋  
```bash
  * storage_directory[/var/opt/gitlab/backups3] action create
    * ruby_block[directory resource: /var/opt/gitlab/backups3] action run
      - execute the ruby block directory resource: /var/opt/gitlab/backups3
```
※ファイルの編集が終わったら、設定を反映させるために$ sudo gitlab-ctl reconfigure　を忘れずに実行しましょう。


## バックアップの保管場所について
バックアップコマンドを実行すると、このようなファイル名のtarファイルが生成されました。  
1531379488_2018_07_12_11.0.3-ee_gitlab_backup.tar  
なお、ファイルサイズはインストールして空のレポジトリを作っただけの状態で、100KB弱でした。  

取得した構成情報のバックアップファイルと、アプリケーションデータのバックアップファイルはセキュリティの観点から、  
別の場所に保管することが望ましいとのことです。保管場所、保管方法についてもよく検討しましょう！  

## 実運用のイメージ(cron利用)

(1)構成情報は週次で、(2)アプリケーションデータは日次でバックアップを取ることを想定してcronを設定してみます。  
うまく動くでしょうか。

### 日次
/etc/cron.daily以下に以下の実行ファイルを配置
```bash
# cat /etc/cron.daily/gitlabrake-backup
echo "######### start daily backup #########" >> /home/vagrant/gitlabrake-backup.log
echo `date` >>  /home/vagrant/gitlabrake-backup.log
sudo gitlab-rake gitlab:backup:create >> /home/vagrant/gitlabrake-backup.log
```
日次バックアップコマンドは標準出力が出てrootにメールが飛ぶので、ログにリダイレクトしています。  
今回、検証環境なので/home以下にログ出力していますが、実運用では/var等になると思います。  
  

### 週次
/etc/cron.weekly以下に以下のコマンドを書いたファイルを配置
```bash
tar cfz /home/vagrant/$(date "+etc-gitlab-%Y%m%d%S.tar.gz") -C / etc/gitlab
```


/etc/cron.d/dailyjobsを編集します。
```bash
# cat /etc/cron.d/dailyjobs
# Run the daily, weekly, and monthly jobs if cronie-anacron is not installed
SHELL=/bin/bash
PATH=/sbin:/bin:/usr/sbin:/usr/bin
MAILTO=root

# run-parts
50 16 * * * root [ ! -f /etc/cron.hourly/0anacron ] && run-parts /etc/cron.daily
00 17 * * 3 root [ ! -f /etc/cron.hourly/0anacron ] && run-parts /etc/cron.weekly
42 10 1 * * root [ ! -f /etc/cron.hourly/0anacron ] && run-parts /etc/cron.monthly
```
日次バックアップは16時50分に、週次バックアップは水曜日(本日)の17時00分に設定しました。


crondを再起動し、待ちます。  
```bash
# systemctl restart crond
```
  
設定した時刻にそれぞれバックアップファイルが取得されていました！
### 日次
デフォルトのディレクトリ、/var/opt/gitlab/backups以下にファイルが作られました。
```bash
# ls -ltr
total 460
-rw-------. 1 git git 92160 Jul 12 13:19 1531369164_2018_07_12_11.0.3-ee_gitlab_backup.tar
-rw-------. 1 git git 92160 Jul 25 16:19 1532503175_2018_07_25_11.0.3-ee_gitlab_backup.tar
-rw-------. 1 git git 92160 Jul 25 16:25 1532503545_2018_07_25_11.0.3-ee_gitlab_backup.tar
-rw-------. 1 git git 92160 Jul 25 16:45 1532504752_2018_07_25_11.0.3-ee_gitlab_backup.tar
-rw-------. 1 git git 92160 Jul 25 17:11 1532506300_2018_07_25_11.0.3-ee_gitlab_backup.tar　 ←　←　←　バックアップファイル
```

### 週次
```bash
# ls -ltr
total 40
drwxrwxr-x. 2 vagrant vagrant   166 Jul 23 17:32 old
-rw-r--r--. 1 root    root     1992 Jul 25 16:45 gitlabrake-backup.log
-rw-r--r--. 1 root    root    30692 Jul 25 17:00 etc-gitlab-2018072501.tar.gz    ←　←　←　バックアップファイル
```
  
  
今回稼働確認のために夕方に取得していますが、実運用では以下のように、火曜～土曜の夜中の1時に取得する(全営業日のバックアップを取得することを意味します)  
といったような利用状況に合わせた設定をすることが多いと思われます。
```bash
0 1 * * 2-6 root [ ! -f /etc/cron.hourly/0anacron ] && run-parts /etc/cron.daily
```



参考サイト：
https://docs.gitlab.com/omnibus/settings/backups.html


## クリエーションラインについて
クリエーションライン株式会社は日本で唯一のGitLab Authorised Partnerです。  
GitLabのサブスクリプション販売、日本語サポートなど、提供サービスの詳細はこちらをご覧ください。  
https://www.creationline.com/gitlab  
