# GitHubのリポジトリをGitLabへインポートする

## GitLabとは

GitLabは、ソースコード管理を中心に開発サイクルの全体（チャット、問題、コードレビュー、CI、CD、サイクルアナリティクス）を統一したUIで提供するプロダクトです。
一般的にGitLabというとGitHubクローンという印象が濃いですが、オープンソースで精力的な開発が行われていて、現在はGitHubで利用できる機能はほぼGitLabでも利用できます。

## GitHubとGitLabでの用語の違いについて

GitHubとGitLabでは、同等の機能であってもその用語が異なっている場合があります。
このことは、GitLabを使いはじめる、GitHubユーザーを困惑させます。
以下は、GitHubとGitLabで用語が異なる、代表的な例です。

| GitHub | GitLab |
| ------ | ------ |
| Repository | Project |
| Pull request | Merge request |
| Organization | Group |
| Gist | Snippet |
| Project | Board |

## GitLab.comについて

簡単に説明すると、GitLab.comはSaaS版のGitLabです。
GitLabはオンプレミスにセルフホスティングして利用するイメージが強いですが、クラウドサービスのGitLab.comも提供されています。
GitLab.comにはいくつかのプランが用意されていますが、基本機能は無料のFreeプランで利用することができます。
また、Freeプランであっても `public` プロジェクトであれば、最上位のGoldプランと同等の機能を利用することができます。
以下は、GitLab.comのすべてのプランに共通する特徴です。

- プライベートプロジェクト数の制限なし
- コラボレーター数の制限なし
- CIパイプラインを無料で利用可能(プランにより実行時間の上限が異なる)

Freeプランと上位プランの違いの詳細は、[公式サイト](https://about.gitlab.com/pricing/#gitlab-com)を参照してください。

## リポジトリのインポート機能について

GitLabには、外部のGitリポジトリをインポートする機能があり、簡単なステップでリポジトリを移行できます。
GitHub、Bitbucket、Google Code、Fogbugz、Gitea、リポジトリURLに対応しています。
このインポート機能を利用すると、リポジトリ以外にも様々なデータを簡単にインポートすることができます。
例えば、GitHubのインポート機能では次のデータがインポートされます。

- Repository description
- Git repository data
- Issue
- Pull request
- Wiki page
- Milestone
- Label
- Release note descriptions
- Pull requestのレビューコメント
- IssueおよびPull requestの通常コメント

インポートしたIssueとPull requestへの参照は保持されます。
また、インポートしたリポジトリの可視性は `private` となるので、必要に応じて `public` に変更してください。

なお、リポジトリのインポートは、セルフホスティングしている場合でも、クラウドサービスを利用している場合でも利用できますが、本記事ではクラウドサービスを利用している場合のインポート方法を紹介します。

## GitLab.comの利用を開始する

リポジトリをインポートするには、まずGitLab.comの利用を開始する必要があります。
そのために、通常はGitLab.comのトップページの https://gitlab.com へアクセスすると思いますが、ログインがしていない状態ではコーポレートサイトの https://about.gitlab.com へリダイレクトされてしまいます。
ここからGitLab.comの「サインイン/ユーザー登録」に進むには、画面右上の「Sign In/Register」をクリックしてください。

![about gitlab.com](images/about_gitlab_com.png)

---

ちなみに、GitLabはGitHubアカウントでのサインインにも対応しています。GitHubアカウントでサインインするには、サインイン画面のGitHubのアイコンをクリックしてください。

![sign in with github](images/sign_in_with_github.png)

---

GitHubアカウントを使用せずに、GitLabアカウントを作成したい場合はサインイン画面の「Register」タブをクリックしユーザー登録を行ってください。
ユーザー登録後、しばらく待つと登録したアドレスにメール届くので、メール内にあるリンクをクリックしてアカウントを有効化してください。

![register gitlab](images/register_gitlab.png)

---

また、初回サインインの時だけサービス利用規約への同意が求められます。リンク先の利用規約を確認の上、問題がなければ「Accept terms」をクリックしてください。

![gitlab terms of service](images/gitlab_terms_of_service.png)

## GitHubのリポジトリをインポートする

GitHubのリポジトリをGitLabにインポートするには、まず画面上部の「New project」をクリックします。

![new project menu](images/new_project_menu.png)

---

続いて、プロジェクトの作成画面の「Import project」タブ -> 「GitHub」をクリックします。

![new project import project](images/new_project_import_project.png)

---

GitHubのリポジトリをGitLabにインポートするには、GitHubのリポジトリをGitLabから取得することを許可する必要があります。
そのため、「List your GitHub repositories」をクリックし、GitLabがGitHubのリポジトリにアクセスすることを許可してください。

![import repositories from github](images/import_repositories_from_github_1.png)

---

GitHubのリポジトリへのアクセスを許可すると、インポートできるリポジトリの一覧が表示されます。
すべてのリポジトリを一括でインポートする場合は、「(1)Import all repositories」をクリックしてください。
特定のリポジトリのみをインポートする場合は、対象リポジトリの「(2)Import」をクリックしてください。

![import repositories from github](images/import_repositories_from_github_2.png)

---

サーバーの混雑状況によっては、インポート処理が完了するまでに数時間かかる場合があるので、処理が完了するまで気長に待ちましょう。
インポートが完了したら、Git repository dataやIssueなどの必要なデータが正しくインポートされていることを確認しておきましょう。

なお、インポート元のGitHubのリポジトリが `Public` の場合でも、インポートされたGitLabのプロジェクトはすべて `Private` となります。
もしインポートしたプロジェクトをメンバー以外にも公開したい場合は、「Settings -> General -> Permissions -> Project visibility」を `Public` に変更してください。

![general settings permissions project visibility.png](images/general_settings_permissions_project_visibility.png)

## 最後に

今回、GitHubのリポジトリをクラウドサービスのGitLab.comにインポートする方法を紹介しましたが、もちろん、セルフホスティングしているGitLabインスタンスにも同じようにインポートすることが可能です。
GitHubのリポジトリをGitLabにインポートするのはとても簡単なので、普段はGitHubを使っているけどGitLabにも興味があるという方は、とりあえず試してみてはいかがでしょうか？

また、クリエーションラインは、日本で唯一のGitLab Authorized Resellerとしてエンタープライズエディションのサブスクリプションの販売や日本語でのサポートを提供しています。
GitLabに関する詳細・お問い合わせは[こちら](https://www.creationline.com/gitlab)を参照ください。
